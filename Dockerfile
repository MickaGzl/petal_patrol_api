FROM node:lts-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install --omit=dev && \
    npm i -g pm2
COPY . .
EXPOSE 3000
CMD ["npm", "run", "prod"]