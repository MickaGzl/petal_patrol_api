# petal_patrol_api

create .env file at the root of the project, and with this keys:

```
DATABASE_PATH=src/db/database/
DB_NAME=sequelize.sqlite
PORT=3000

JWT_SECRET_KEY=
NODEMAILER_HOST=
NODEMAILER_PORT=
NODEMAILER_USER=
NODEMAILER_PASSWORD=
TLS_REJECT=false
```
