import express from "express";
import cors from "cors";
import { rateLimit } from "express-rate-limit";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";
import { join } from "path";
import { fileURLToPath } from "url";
import { readFileSync } from "fs";
import { closeDb, connection } from "./src/db/server.js";
import { createCsrfSecret } from "./src/config/csrfConfig.js";
import {
  extractUserFromToken,
  addJwtFeatures,
} from "./src/config/jwtConfig.js";
import { router } from "./src/routes/index.js";
import swaggerUi from "swagger-ui-express";

dotenv.config();
console.log("DATABASE: ", process.env.POSTGRES_USER);

const documentationData = readFileSync(
  join(fileURLToPath(import.meta.url), "../src/config/swagger-output.json")
);
const documentation = JSON.parse(documentationData);

export const app = express();
// module.exports.app = app;

//to have a view on template emails
app.use(express.static("public"));
app.set("view engine", "pug");
app.set("views", join(fileURLToPath(import.meta.url), "../src/views/"));

process.env.CSRF_SECRET = createCsrfSecret();

if (process.env.NODE_ENV.trim() !== "test") {
  connection();
}

//create a window of 10min. each ip have the right to make 100 request during this window

const limiter = rateLimit({
  windowMs: 10 * 60 * 1000,
  max: 100,
  standardHeaders: true,
  legacyHeaders: false,
});

app
  .use(limiter)
  .use(
    cors({
      origin: [
        "http://localhost:5173",
        "http://localhost:3000",
        "http://localhost:8081",
        "http://192.168.1.24:8081", //Marilyn
        "http://192.168.1.34:8081",
        "http://172.20.10.2:8081",
        "http://192.168.1.48:8081", //Thierry
        "http://172.20.10.3:8081",
        "http://192.168.235.199:8081", //Roxane
        "http://192.168.168.199:8081",
        "http://192.168.117.199:8081",
        "http://192.168.1.15:8081",
        "http://*:8081",
      ],
      credentials: true,
    })
  )
  .use(express.json())
  .use(express.urlencoded({ extended: false }))
  .use(cookieParser())
  .use(extractUserFromToken)
  .use(addJwtFeatures)
  .use(
    "/images/images",
    express.static(
      join(fileURLToPath(import.meta.url), "../public/assets/images")
    )
  )
  .use(
    "/images/users",
    express.static(
      join(fileURLToPath(import.meta.url), "../public/assets/users")
    )
  )
  .use(
    "/images/plants",
    express.static(
      join(fileURLToPath(import.meta.url), "../public/assets/plants")
    )
  )
  .use(
    "/images/rapports",
    express.static(
      join(fileURLToPath(import.meta.url), "../public/assets/rapports")
    )
  )
  .use("/documentation", swaggerUi.serve, swaggerUi.setup(documentation))
  .use(router);

const port = process.env.PORT || 3000;

// const host =
// process.env.NODE_ENV.trim() === "production" ? "0.0.0.0" : "127.0.0.1";
if (process.env.NODE_ENV !== "test") {
  const server = app.listen(
    port,
    console.log(`app listen on port http://localhost:${port}`)
  );

  process.addListener("SIGINT", async () => {
    try {
      server.close();
      await closeDb();

      process.exit(0);
    } catch (error) {
      process.exit(1);
    }
  });
}

//update plant create with multer store files, just send object with name, type and image

//validate_account = n'empeche pas de se connecter mais d'enregistrer des offres ou de répondres à celles ci

//invalid user return true a validate_account pour test, l'enlever, et remettre le mail dans create user
