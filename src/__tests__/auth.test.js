import supertest from "supertest";
import { app } from "../../app.js";
import { jest } from "@jest/globals";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

jest.mock("../db/server.js");
import { User, Role } from "../db/server.js";

const BASE_USER = { name: "gigi", email: "mickael.gonzales30@outlook.fr" };

/**
 * shortest call to jest.spyOn
 * @param {Object} object The Object called the method need to be mocked
 * @param {string} method The method Jest need to mock
 * @param {*} returnedValue the value you want the spy method return
 * @param {boolean} promise boolean needed if the method doesn't return a Promise
 * @returns jest.spyOn(object, method).mockResonledValue(returnedValue)
 */
const mock = (object, method, returnedValue, unPromise = false) => {
  if (unPromise) {
    return jest.spyOn(object, method).mockReturnValue(returnedValue);
  }
  return jest.spyOn(object, method).mockResolvedValue(returnedValue);
};

const req = supertest(app);

describe("test auth functions", () => {
  test("Api is reachable", async () => {
    const res = await req.get("/test");
    console.log(res.request.url);

    expect(res.status).toEqual(200);
    expect(res.type).toEqual(expect.stringContaining("json"));
    expect(res.body).toEqual({ message: "hello world" });
  });

  test("create user", async () => {
    mock(User, "findOne", null);
    mock(User, "create", {
      ...BASE_USER,
      validate_account: false,
      activation_token: "randomString",
      addRole: jest.fn(),
      save: jest.fn().mockReturnValue({
        ...BASE_USER,
        activation_token: "randomString",
      }),
    });
    mock(Role, "findOne", { id: 1, role: "USER" });

    const res = await req
      .post("/api/auth/signup")
      .send({ ...BASE_USER, password: "totototo" })
      .set("Content-Type", "application/json");

    expect(res.status).toEqual(200);
    expect(res.body).toEqual({
      message:
        "Enregistré avec succès. Veuillez vérifier vos mails afin de valider votre compte avant de vous connecter.",
      user: BASE_USER,
    });
  });

  test("do not create user if email already exist in db", async () => {
    mock(User, "findOne", BASE_USER);

    const res = await req
      .post("/api/auth/signup")
      .send({ ...BASE_USER, password: "totototo" })
      .set("Content-Type", "application/json");

    expect(res.status).toEqual(409);
    expect(res.body).toEqual({
      message: "Cet email est déjà utilisé.",
    });
  });

  test("signin to application", async () => {
    mock(User, "findOne", {
      ...BASE_USER,
      password: "hashedToto",
      validate_account: true,
      save: jest.fn(),
    });
    mock(Role, "findAll", [{ dataValues: { role: "USER" } }]);
    mock(bcrypt, "compare", true);
    mock(jwt, "sign", { sub: 1, exp: 123456789 }, true);

    const res = await req.post("/api/auth/signin").send({
      email: "titi@mail.com",
      password: "toto",
    });

    expect(res.status).toEqual(200);
    expect(res.body.user.password).toEqual("");
    expect(res.headers["set-cookie"][0].startsWith("jwt=")).toBeTruthy();
  });

  test("signout user by deleting the authentication cookie", async () => {
    const res = await req.get("/api/auth/signout");

    expect(res.status).toEqual(200);
    expect(res.headers["set-cookie"]).toEqual([
      "jwt=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT",
    ]);
    expect(res.body).toEqual({
      message: "Déconnexion réussi.",
    });
  });

  test("Verify server tokens validity", async () => {
    const res = await req.get("/api/auth/token");
    const token = res.body.token;
    const res2 = await req
      .post("/api/auth/verify-server-token")
      .set("Content-Type", "application/json")
      .send({ csrfToken: token });

    expect(res.status).toEqual(200);
    expect(res2.body).toEqual({ message: "ok." });
  });

  test("server tokens invalid", async () => {
    const res = await req
      .post("/api/auth/verify-server-token")
      .set("Content-Type", "application/json")
      .send({ csrfToken: "invalidToken" });

    expect(res.status).toEqual(401);
    expect(res.body).toEqual({
      token: "invalidToken",
      message: "Token invalide.",
    });
  });
});
