import supertest from "supertest";
import { app } from "../../app.js";
import { jest } from "@jest/globals";
import jwt from "jsonwebtoken";

jest.mock("../db/server.js");
import { User, Role, Offer } from "../db/server.js";

const req = supertest(app);

/**
 * shortest call to jest.spyOn
 * @param {Object} object The Object called the method need to be mocked
 * @param {string} method The method Jest need to mock
 * @param {*} returnedValue the value you want the spy method return
 * @param {boolean} promise boolean needed if the method doesn't return a Promise
 * @returns jest.spyOn(object, method).mockResonledValue(returnedValue)
 */
const mock = (object, method, returnedValue, unPromise = false) => {
  if (unPromise) {
    return jest.spyOn(object, method).mockReturnValue(returnedValue);
  }
  return jest.spyOn(object, method).mockResolvedValue(returnedValue);
};

describe("test offers functions", () => {
  beforeEach(() => {
    mock(User, "findByPk", { id: 2, name: "Tata" });
    mock(Role, "findAll", [{ dataValues: { role: "USER" } }]);
    mock(jwt, "verify", { sub: 2 }, true);
  });
  test("get all offers active on site", async () => {
    mock(Offer, "findAll", [
      {
        id: 1,
        description: "Plante à garder",
        coordinates: '{"lat": 43.6, "lng": 3.8}',
        plant: {
          images: '[{"p1": "a"}, {"p2": "b"}]',
          dataValues: { id: 1, name: "tata" },
        },
      },
      {
        id: 2,
        description: "Plante à garder",
        coordinates: '{"lat": 43.6, "lng": 3.8}',
        plant: {
          images: '[{"p1": "a"}, {"p2": "b"}]',
          dataValues: { id: 2, name: "titi" },
        },
      },
    ]);

    const res = await req.get("/api/offer").set("Cookie", "jwt=someChars");

    expect(res.status).toEqual(200);
    expect(res.body.offers).toHaveLength(2);
  });
});
