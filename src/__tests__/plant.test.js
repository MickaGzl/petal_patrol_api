import supertest from "supertest";
import { app } from "../../app.js";
import { jest } from "@jest/globals";
import jwt from "jsonwebtoken";

jest.mock("../db/server.js");
import { User, Role, Plant } from "../db/server.js";

const req = supertest(app);

/**
 * shortest call to jest.spyOn
 * @param {Object} object The Object called the method need to be mocked
 * @param {string} method The method Jest need to mock
 * @param {*} returnedValue the value you want the spy method return
 * @param {boolean} promise boolean needed if the method doesn't return a Promise
 * @returns jest.spyOn(object, method).mockResonledValue(returnedValue)
 */
const mock = (object, method, returnedValue, unPromise = false) => {
  if (unPromise) {
    return jest.spyOn(object, method).mockReturnValue(returnedValue);
  }
  return jest.spyOn(object, method).mockResolvedValue(returnedValue);
};

const ALL_PLANTS = [
  { id: 1, name: "tata", images: '[{"p1": "a"}, {"p2": "b"}]' },
  { id: 2, name: "toto", images: '[{"p1": "a"}, {"p2": "b"}]' },
  { id: 3, name: "titi", images: '[{"p1": "a"}, {"p2": "b"}]' },
];

describe("test the plant functions", () => {
  beforeEach(() => {
    mock(User, "findByPk", { id: 2, name: "Tata" });
    mock(Role, "findAll", [{ dataValues: { role: "USER" } }]);
    mock(jwt, "verify", { sub: 2 }, true);
  });

  test("acces to all the registered plants if user authenticated is admin", async () => {
    mock(User, "findByPk", { id: 1, name: "Toto" });
    mock(Role, "findAll", [
      { dataValues: { role: "USER" } },
      { dataValues: { role: "ADMIN" } },
    ]);
    mock(jwt, "verify", { sub: 1 }, true);
    mock(Plant, "findAll", ALL_PLANTS);

    const res = await req.get("/api/plant").set("Cookie", "jwt=someChars");

    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("plants");
    expect(res.body.plants[0].images).toEqual([{ p1: "a" }, { p2: "b" }]);
  });

  test("find plants corresponding to user", async () => {
    mock(Plant, "findAll", ALL_PLANTS);
    const res = await req.get("/api/plant/my").set("Cookie", "jwt=someChars");

    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("plants");
    expect(res.body.plants[0].images).toEqual([{ p1: "a" }, { p2: "b" }]);
  });
});
