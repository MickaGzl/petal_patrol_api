import supertest from "supertest";
import { app } from "../../app";
import { jest } from "@jest/globals";
import jwt from "jsonwebtoken";
import { Role, User } from "../db/server";
import { emailFactory } from "../mailer";

const req = supertest(app);

jest.mock("../mailer", () => ({
  sendResetPasswordLink: jest.fn(),
  sendEmailVerificationLink: jest.fn(),
}));

jest.mock("jsonwebtoken", () => ({
  sign: jest.fn(() => "fakeToken"),
  verify: jest.fn(() => ({ sub: 1 })),
}));

const USER_TEST = {
  id: 2,
  name: "Tata",
  email: "tata@mail.com",
  activation_token: "",
};

/**
 * shortest call to jest.spyOn
 * @param {Object} object The Object called the method need to be mocked
 * @param {string} method The method Jest need to mock
 * @param {*} returnedValue the value you want the spy method return
 * @param {boolean} promise boolean needed if the method doesn't return a Promise
 * @returns jest.spyOn(object, method).mockResonledValue(returnedValue)
 */
const mock = (object, method, returnedValue, unPromise = false) => {
  if (unPromise) {
    return jest.spyOn(object, method).mockReturnValue(returnedValue);
  }
  return jest.spyOn(object, method).mockResolvedValue(returnedValue);
};

describe("test the user's functions", () => {
  test("cannot access to all user informations if not administrator", async () => {
    const res = await req.get("/api/user");
    expect(res.status).toEqual(401);
    expect(res.body.message).toEqual(
      "Vous ne pouvez pas accéder à cette ressource"
    );
    expect(res.body.users).not.toBeDefined();
  });

  test("acces to all users if user authenticated is admin", async () => {
    mock(User, "findByPk", { id: 1, name: "Toto" });
    mock(User, "findAll", [
      {
        dataValues: {
          id: 1,
          name: "toto",
          roles: [{ role: "USER" }, { role: "ADMIN" }],
        },
      },
      {
        dataValues: {
          id: 2,
          name: "tata",
          roles: [{ role: "USER" }],
        },
      },
    ]);
    mock(Role, "findAll", [
      { dataValues: { role: "USER" } },
      { dataValues: { role: "ADMIN" } },
    ]);
    mock(jwt, "verify", { sub: 1 }, true);

    const res = await req.get("/api/user").set("Cookie", "jwt=someChars");

    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("users");
  });

  test("acces to one user if user authenticated is admin", async () => {
    mock(User, "findByPk", { id: 1, name: "Toto" });
    mock(Role, "findAll", [
      { dataValues: { role: "USER" } },
      { dataValues: { role: "ADMIN" } },
    ]);
    mock(jwt, "verify", { sub: 1 }, true);
    mock(User, "findOne", {
      dataValues: {
        id: 2,
        name: "tata",
        roles: [{ role: "USER" }],
      },
    });

    const res = await req.get("/api/user/2").set("Cookie", "jwt=someChars");

    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty("user");
  });

  test("update user email invalidate it", async () => {
    mock(User, "findByPk", {
      ...USER_TEST,
      save: jest.fn().mockResolvedValue({
        id: 2,
        name: "titi",
        email: "titi@newmail.com",
        validate_account: false,
      }),
      dataValues: USER_TEST,
    });
    mock(Role, "findAll", [{ dataValues: { role: "USER" } }]);
    mock(jwt, "verify", { sub: 2 }, true);

    const res = await req
      .put("/api/user/2")
      .send({
        name: "titi",
        email: "titi@newmail.com",
      })
      .set("Cookie", "jwt=someChar");

    expect(res.status).toEqual(200);
    expect(res.body.user).toEqual({
      id: 2,
      name: "titi",
      validate_account: false,
      email: "titi@newmail.com",
    });
  });

  test("user delete his account", async () => {
    mock(User, "findByPk", { ...USER_TEST, save: jest.fn() });
    mock(Role, "findAll", [{ dataValues: { role: "USER" } }]);
    mock(jwt, "verify", { sub: 2 }, true);

    const res = await req.delete("/api/user/").set("Cookie", "jwt=someChar");

    const deletedDate = new Date(
      Date.now() + 1000 * 60 * 60 * 24 * 30
    ).toLocaleDateString();
    expect(res.status).toEqual(200);
    expect(new Date(res.body.accountDeletedOn).toLocaleDateString()).toEqual(
      deletedDate
    );
  });

  test("user ask for reset password", async () => {
    mock(User, "findOne", { id: 2, save: jest.fn() });
    mock(emailFactory, "sendResetPasswordLink");
    const res = await req
      .post("/api/user/reset-password")
      .send({ email: "titi@mail.com" });

    expect(res.status).toEqual(200);
  });

  test("reset password with unverified perished token", async () => {
    mock(User, "findByPk", {
      id: 2,
      password_token: "notCorrectToken",
      password_token_expiration: Date.now() + 1000 * 60,
      save: jest.fn(),
    });

    const res = await req
      .post("/api/user/reset-password/2/tokenForPassword")
      .send({ password: "newSuperPassword123" });

    expect(res.status).toEqual(404);
    expect(res.body).toEqual({
      message:
        "le token fournit est invalide. Il est possible qu'il soit expiré.",
    });
  });

  test("user reset his password", async () => {
    mock(User, "findByPk", {
      id: 2,
      password_token: "tokenForPassword",
      password_token_expiration: Date.now() + 1000 * 60,
      save: jest.fn(),
    });

    const res = await req
      .post("/api/user/reset-password/2/tokenForPassword")
      .send({ password: "newSuperPassword123" });

    expect(res.status).toEqual(200);
  });
});
