import CsrfToken from "csrf";

const token = new CsrfToken();

/**
 * create a secret that will serve to create tokens and verify authenticity
 */
export function createCsrfSecret() {
  return token.secretSync();
}

/**
 * create a token with the secret string generated by app
 * @param {string} secret
 * @returns the token created
 */
export function createTokenFromSecret(secret) {
  return token.create(secret);
}

/**
 * compare the received token with the secret
 * @param {string} secret
 * @param {string} receivedToken
 * @returns true if the authenticity of the token is prouved
 */
export function validateTokenWithSecret(secret, receivedToken) {
  return token.verify(secret, receivedToken);
}
