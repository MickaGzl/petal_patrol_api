import swaggerAutogen from "swagger-autogen";

const doc = {
  info: {
    title: "MSPR Petal Patrol API",
    description: "API for communication with database in Petral Patrol Project",
  },
  servers: [
    {
      url: "http://localhost:3000/",
      description: "local development server",
    },
    {
      url: "https://mspr.mickael-gonzales.fr",
      description: "main production server",
    },
  ],
  definition: {
    SignInUser: {
      $message: "Connexion réussi",
      $user: {
        id: 10,
        name: "toto",
        email: "user@email.com",
        avatar: null,
        validate_account: false,
        lastLog: "1970-01-01T00:00:00.000Z",
        deletedOn: null,
        updatedAt: "1970-01-01T00:00:00.000Z",
        roles: ["USER"],
      },
    },
    ErrorServer: {
      $message: "Une erreur est survenue lors de votre action.",
    },
  },
};

const outputFile = "./swagger-output.json";
const routes = ["../routes/index.js"];

swaggerAutogen({ openapi: "3.0.0" })(outputFile, routes, doc);
