import { Sequelize, DataTypes } from "sequelize";
import * as models from "../models/index.js";
import dotenv from "dotenv";
import { applyAssociations } from "../setup/associations.js";

dotenv.config();

const dialects = [
  "mysql",
  "postgres",
  "sqlite",
  "mariadb",
  "mssql",
  "db2",
  "snowflake",
  "oracle",
];

//define model order creation, to prevent forein eky contraint error
const ModelsOrder = [
  "role",
  "user",
  "userRole",
  "waitingBotanist",
  "plant",
  "offer",
  "proposal",
  "rapport",
  "comment",
];

/**
 * create all model of app, apply associations and synchronise database sended in params
 * @param {database} database the sgbd concerned
 * @returns the synchronisation of models to db
 */
async function createModels(database, recreate = false) {
  for (const key in models) {
    models[key](database, DataTypes);
  }
  applyAssociations(database);
  return database.sync({ force: recreate });
}

async function makeMigration(argvs) {
  //verify all argvs has been passed
  if (argvs.length !== 5) {
    console.error(
      'Argument is missing. Please run "npm run migrate <dbname> <username> <password> <dialect> <host>". '
    );
    process.exit(1);
  }
  const [dbname, username, password, dialect, host] = argvs;
  if (!dialects.includes(dialect)) {
    console.error(
      `dialect incorrect. Please use a relationnal database from the followings: ${dialects.join(
        " | "
      )}`
    );
    process.exit(1);
  }

  console.log(`run migration on ${dialect}:${dbname}`);

  const sqliteDB = new Sequelize({
    dialect: "sqlite",
    storage: `${process.env.DATABASE_PATH}${process.env.DB_NAME}`,
    logging: false,
  });

  const newDB = new Sequelize(dbname, username, password, {
    host,
    dialect,
    logging: false,
  });

  await createModels(sqliteDB);
  await createModels(newDB, true);

  for (const model of ModelsOrder) {
    const rows = await sqliteDB.models[model].findAll();

    await Promise.all(
      rows.map(async (row) => {
        await newDB.models[model].create(row.toJSON());
      })
    );
    console.log(
      `Succesfully inserted model ${model} into ${dialect} database.`
    );
  }
  console.log("migration complete");
}

makeMigration(process.argv.splice(2));
