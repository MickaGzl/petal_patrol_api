import crypto from "crypto";
import { config } from "dotenv";
config();

export function encryptData(data) {
  const dataToEncrypt = typeof data === "object" ? JSON.stringify(data) : data;

  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv(
    process.env.ENCRYPT_ALGORITHM,
    process.env.ENCRYPT_KEY,
    iv
  );
  let encryptedData = cipher.update(dataToEncrypt, "utf8", "hex");
  encryptedData += cipher.final("hex");
  return [iv, encryptedData];
}

export function decryptData(data, iv) {
  const decipher = crypto.createDecipheriv(
    process.env.ENCRYPT_ALGORITHM,
    process.env.ENCRYPT_KEY,
    iv
  );
  let decryptedData = decipher.update(data, "hex", "utf8");
  decryptedData += decipher.final("utf8");
  return decryptedData;
}
