import { Op } from "sequelize";
import { Offer, Plant, User } from "../db/server.js";

const DEFAULT_OPTIONS = {
  order: [["id", "DESC"]],
  include: [
    {
      model: User,
      as: "owner",
      attributes: ["name", "avatar", "id"],
    },
    {
      model: User,
      as: "guardian",
      attributes: ["name", "avatar", "id"],
    },
    {
      model: Plant,
      attributes: ["name", "type", "id", "images"],
    },
  ],
};

/**
 * return a list of offers registered in app
 * @param {Object} query queryParams send in url
 * @param {boolean} forUser request made by user (only send active offers)
 * @returns all offers corresponding to queries
 */
export const findAllOffer = (query, forUser) => {
  let options = { ...DEFAULT_OPTIONS };

  if (forUser || query.active) {
    options = {
      ...options,
      where: {
        [Op.and]: [
          { guardianId: { [Op.is]: null } },
          { complete: false },
          { date_to: { [Op.gt]: Date.now() } },
        ],
      },
    };
  }

  if (query && query.city) {
    options = {
      ...options,
      where: { ...options.where, city: { [Op.like]: `%${query.city}%` } },
    };
  }

  if (query && query.plant) {
    options.include[2] = {
      ...options.include[2],
      where: { type: { [Op.like]: `%${query.plant}%` } },
    };
  }

  return Offer.findAll(options);
};

export const findOfferByUserId = (query, userId) => {
  let options = {
    ...DEFAULT_OPTIONS,
    where: { ownerId: { [Op.eq]: userId } },
  };

  if (query && query.active) {
    options = {
      ...options,
      where: {
        ...options.where,
        complete: false,
        date_to: { [Op.gt]: Date.now() },
      },
    };
  }

  return Offer.findAll(options);
};

export const findOfferByGuardianId = (userId) => {
  return Offer.findAll({
    ...DEFAULT_OPTIONS,
    where: { guardianId: { [Op.eq]: userId } },
  });
};

/**
 * find all offer corresponding to a plant
 */
export const findOfferByPlantId = (plantId) => {
  return Offer.findAll({ where: { plantId: { [Op.eq]: plantId } } });
};

export const findOfferById = (id) => {
  return Offer.findOne({ ...DEFAULT_OPTIONS, where: { id } });
};

export const createOffer = async (offer, userId) => {
  const newOffer = await Offer.create({
    ...offer,
  });
  newOffer.setOwner(userId);
  return newOffer.save();
};

export const updateOffer = async (offer, newValues) => {
  return offer.update(newValues);
};

export const deleteOffer = (id) => {
  return Offer.destroy({ where: { id } });
};

export const findCurrentOfferWithUser = (userId) => {
  const options = {
    where: {
      guardianId: { [Op.not]: null },
      date_to: { [Op.gte]: Date.now() - (1000 * 60 * 60 * 24 - 3) },
      [Op.or]: [
        { ownerId: { [Op.eq]: userId } },
        { guardianId: { [Op.eq]: userId } },
      ],
    },
    attributes: ["id"],
  };

  return Offer.findAll(options);
};
