import { Router } from "express";
import { router as userRoute } from "./userRoute.js";
import { router as authRoute } from "./authRoute.js";
import { router as plantRoute } from "./plantRoute.js";
import { router as waitingBotanistRoute } from "./waitingBotanistRoute.js";
import { router as dataRoute } from "./dataRoute.js";
import { router as offerRoute } from "./offerRoute.js";
import { router as proposalRoute } from "./proposalRoute.js";
import { router as rapportRoute } from "./rapportRoute.js";
import { router as commentRoute } from "./commentRoute.js";
import {
  ensureIsAuthenticated,
  ensureUserHaveRights,
} from "../../config/authConfig.js";

export const router = Router();

router.use(
  "/auth",
  // #swagger.tags = ['Authentification']
  authRoute
);
router.use(
  "/user",
  // #swagger.tags = ['User']
  userRoute
);
router.use(
  "/plant",
  // #swagger.tags = ['Plant']
  plantRoute
);
router.use(
  "/offer",
  // #swagger.tags = ['Offer']
  offerRoute
);
router.use(
  "/proposal",
  // #swagger.tags = ['Proposal']
  proposalRoute
);
router.use(
  "/rapport",
  // #swagger.tags = ['Rapport']
  rapportRoute
);
router.use(
  "/comment",
  // #swagger.tags = ['Comment']
  commentRoute
);
router.use(
  "/waiting-list",
  // #swagger.tags = ['Botanist List']
  ensureIsAuthenticated,
  ensureUserHaveRights,
  waitingBotanistRoute
);
router.use(
  "/data",
  // #swagger.tags = ['Data']
  dataRoute
);
